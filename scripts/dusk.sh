#!/bin/sh

wait-for.sh web:80 || { echo "Could not reach NGINX in time"; exit 1; }
wait-for.sh $REDIS_HOST:$REDIS_PORT || { echo "Could not reach Redis in time"; exit 1; }
wait-for.sh $DB_HOST:$DB_PORT || { echo "Could not reach Database in time"; exit 1; }
wait-for.sh chrome:4444 || { echo "Could not reach Selenium in time"; exit 1; }
wait-for.sh app:9000 || { echo "Could not reach Redis in time"; exit 1; }

php artisan dusk $@
