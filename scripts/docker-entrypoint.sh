#!/bin/sh

wait-for.sh "$REDIS_HOST:$REDIS_PORT" || { echo "Could not reach Redis in time"; exit 1; }
wait-for.sh "$DB_HOST:$DB_PORT" || { echo "Could not reach Database in time"; exit 1; }
if [ ! -z "$VARNISH_HOST" ] && [ ! -z "$VARNISH_PORT" ]; then
    wait-for.sh "$VARNISH_HOST:$VARNISH_PORT" || { echo "Could not reach Varnish in time"; exit 1; }
fi

if [ "$APP_ENV" != "local" ] && [ "$APP_ENV" != "testing" ]; then
    su web -c "php artisan config:cache || exit 1; php artisan route:cache || exit 1" || exit 1
fi

if [ "$APP_ENV" != "testing" ]; then
    su web -c "php artisan migrate --force || exit 1; php artisan db:seed --force || exit 1" || exit 1
fi

exec php-fpm
