#!/bin/sh

wait-for.sh $REDIS_HOST:$REDIS_PORT || { echo "Could not reach Redis in time"; exit 1; }
wait-for.sh $DB_HOST:$DB_PORT || { echo "Could not reach Database in time"; exit 1; }

while :
do
    sleep 1m;
    php artisan schedule:run &
done
