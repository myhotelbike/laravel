#!/bin/sh

wait-for.sh $REDIS_HOST:$REDIS_PORT || { echo "Could not reach Redis in time"; exit 1; }
wait-for.sh $DB_HOST:$DB_PORT || { echo "Could not reach database in time"; exit 1; }

php artisan horizon $@
